package main

import (
	"bitbucket.org/lcvinny/goose/lib/goose"
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

var mergeCmd = &Command{
	Name:    "merge",
	Usage:   "",
	Summary: "Merge by moving unapplied migrations after last",
	Help:    `merge extended help here...`,
	Run:     mergeRun,
}

type mergeData struct {
	Source string
	merge  string
}

var inc int64

func mergeRun(cmd *Command, args ...string) {

	if len(args) < 1 {
		log.Fatal("goose merge: last migration required")
	}

	last, err := strconv.ParseInt(args[0], 10, 64)
	if err != nil {
		log.Fatal(err)
	}

	conf, err := dbConfFromFlags()
	if err != nil {
		log.Fatal(err)
	}

	// collect all migrations

	migrations, e := goose.CollectMigrations(conf.MigrationsDir, 0, math.MaxInt64)
	if e != nil {
		log.Fatal(e)
	}

	db, e := goose.OpenDBFromDBConf(conf)
	if e != nil {
		log.Fatal("couldn't open DB:", e)
	}
	defer db.Close()

	// must ensure that the version table exists if we're running on a pristine DB
	if _, e := goose.EnsureDBVersion(conf, db); e != nil {
		log.Fatal(e)
	}

	fmt.Printf("goose: merge for environment '%v'\n", conf.Env)
	fmt.Println(last, "last")

	for _, m := range migrations {
		printMigrationmerge(db, last, m.Version, filepath.Base(m.Source))
	}
}

func printMigrationmerge(db *sql.DB, last int64, version int64, script string) {
	var row goose.MigrationRecord
	q := fmt.Sprintf("SELECT tstamp, is_applied FROM goose_db_version WHERE version_id=%d ORDER BY tstamp DESC LIMIT 1", version)
	e := db.QueryRow(q).Scan(&row.TStamp, &row.IsApplied)

	if e != nil && e != sql.ErrNoRows {
		log.Fatal(e)
	}

	if !row.IsApplied {

		if version < last {
			inc++
			from := version
			version = last + inc
			fmt.Println(version, "<-", from)

			toName := strings.Replace(script, strconv.FormatInt(from, 10), strconv.FormatInt(version, 10), 1)
			newPath := filepath.Join(*flagPath, "migrations", toName)
			err := os.Rename(filepath.Join(*flagPath, "migrations", script), newPath)
			if err != nil {
				log.Fatal(err)
			}

			if filepath.Ext(script) == ".go" {
				buf, err := ioutil.ReadFile(newPath)
				if err != nil {
					log.Fatal(err)
				}
				buf = []byte(strings.Replace(string(buf), strconv.FormatInt(from, 10), strconv.FormatInt(version, 10), 2))
				err = ioutil.WriteFile(newPath, buf, 0644)
				if err != nil {
					log.Fatal(err)
				}
			}

		} else {
			fmt.Println(version, "after last")
		}
	}
}
